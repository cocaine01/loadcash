package pattern;

import java.util.Scanner;

public class FloydTriangle {

	public static void main(String[] args) {
		int n;
		int num = 1;
		int c, d;
	      Scanner in = new Scanner(System.in);
	 
	      System.out.println("Enter the number of rows of floyd's triangle you want");
	      n = in.nextInt();
	 
	      System.out.println("Floyd's triangle :-");
	      int k  = 1;
	      for ( c = 1 ; c <= n ; c++ )
	      {
	         for ( d = 1 ; d <= c ; d++ )
	         {
	            System.out.print(k +"\t");//space
	            k = k+1;	            
	         }
	 
	         System.out.print("\n");//new line
	      }
	   }
	}