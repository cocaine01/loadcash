package LoadCash.LoadCash;



import org.testng.annotations.Test;
import org.testng.AssertJUnit;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import PageObjects.HomePage;

public class TestLoad extends Base{
	
	WebDriver driver;
	HomePage homepage = null;
	
	@BeforeMethod
	public void beforeMethod() throws Exception{
		loadProperties() ;
		driver = Base.getBrowser(CONFIG.getProperty("browser"));
		driver.get(CONFIG.getProperty("url"));
	}
		
	@Test(description = "Intelligent code compilation")
	public void test() throws Exception{
		homepage = PageFactory.initElements(driver,HomePage.class );
		AssertJUnit.assertTrue(homepage.LoadCashMtd());
	}
	
	

}
