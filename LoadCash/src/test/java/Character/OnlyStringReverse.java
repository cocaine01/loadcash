package Character;

import java.util.HashMap;
import java.util.Map;

public class OnlyStringReverse {

	public static void main(String[] args) {
		String str ="Mumbai Goa Pune Mumbai";
		
//		for(int i=str.length()-1;i>=0;i--) {
//			System.out.print(str.charAt(i));
//		}

		String[] ar=str.split(" ");
		//Reversing the sequence
		for(int i=ar.length-1;i>=0;i--) {
			System.out.println(ar[i]);
		}
		
		//Reversing the characters in teh words
		for(String str3:ar) {
			char[] c=str3.toCharArray();
			for(int j=c.length-1;j>=0;j--) {
				System.out.print(c[j]);
			}
			System.out.print(" ");
		}
		///count of words
		int count=1;
		for(int i=0;i<=str.length()-1;i++) {
			if(str.charAt(i)==' ' && str.charAt(i+1)!= ' ') {
				count=count+1;
			}
		}
		System.out.println(count);
		
		
		//Print only first character PrintFirstCharacterOFEveryWordinString	
		char[] ch=str.toCharArray();
		
		System.out.println(ch[0]);
		
		for(int i=0;i<=ch.length-1;i++) {
			if (ch[i]==' ') {
				System.out.print(ch[i+1]);
			}
		}
		
		//Repeating words
		String[] arr1=str.split(" ");
		
		Map<String,Integer> hm= new HashMap<>();
		
		for(String temp:arr1) {
			if(hm.get(temp)!=null) {
				hm.put(temp, hm.get(temp)+1);
			}
			else {
				hm.put(temp, 1);
			}
		}
		System.out.println(hm);		
	}

}
