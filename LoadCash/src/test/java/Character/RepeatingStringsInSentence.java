package Character;

import java.util.HashMap;
import java.util.Map;

public class RepeatingStringsInSentence {

	public static void main(String[] args) {
		String str = "Sumeet Kumar is is a a boy boy";
		
		String [] arr=str.split(" ");
		
		Map<String,Integer> hm =new HashMap<>();
		
		for(String temp : arr) {
			if(hm.get(temp)!= null) {
				hm.put(temp, hm.get(temp)+1);
			}
			else {
				hm.put(temp, 1);
			}
		}
		
		System.out.println(hm);
		
		for(Map.Entry<String, Integer> mp :hm.entrySet())
		{
			System.out.println(mp.getKey()+" "+mp.getValue());
		}
		System.out.println("------------------");
		
		Map<Character,Integer> hm2 =new HashMap<>();
	    
		String[] str2 =str.split(" ");
		int count=0;
		for(String str3 :str2) {
			
			for(int i=0;i<=str3.length()-1;i++) {
				for(int j=0;j<=str3.length()-1;j++) {
					if(str3.charAt(i)==str3.charAt(j)) {
						count=count+1;
					}					
				}
				if(count>0) {
					hm2.put(str3.charAt(i), count);
				}
				else {
					hm2.put(str3.charAt(i), 1);
				}
				count=0;
			}
					
		}
		System.out.println(hm2);
	
	
	}

}
