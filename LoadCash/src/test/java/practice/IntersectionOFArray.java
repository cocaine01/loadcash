package practice;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class IntersectionOFArray {

	public static void main(String[] args) {
	    List<Object> al1 = new ArrayList<>();
	    List<Object> al2 = new ArrayList<>();
	    HashMap<Object, Integer> hm = new HashMap<Object, Integer>();
	    int k=0;
	    
	    al1.add(1);
	    al1.add(2);
	    al1.add(1);
	    al1.add("Amit");
	    
	    al2.add(2);
	    al2.add(3);
	    al2.add(4);
	    al2.add("Amit");
	    
	    System.out.println(al1);
	    System.out.println(al2);
	    
	    StringBuilder sb1 = new StringBuilder();
	    for (Object s1 : al1)
	    {
	        sb1.append(s1);	        
	    }
	    System.out.println(sb1);
	    
	    StringBuilder sb2 = new StringBuilder();
	    for (Object s2 : al2)
	    {
	        sb2.append(s2);	        
	    }
	    System.out.println(sb2);
	    
	    for(int i=0;i<=sb1.length()-1;i++){
	    	for (int j=0;j<=sb2.length()-1;j++){
	    		
	    		if(sb1.charAt(i)==sb2.charAt(j))
	    		{
	    			k=k+1;
	    		}
	    		
	    	}
	    	if (k>0){
	    		hm.put(sb1.charAt(i), k);
	    	}
	    	else{
	    		hm.put(sb1.charAt(i), 1);
	    	}
	    	k=0;
	    }
	    System.out.println(hm);
	    for (Map.Entry<Object, Integer> abc : hm.entrySet()){
	    	System.out.println(abc.getKey()+"---->"+abc.getValue());
	    }
	    
	    List<String> lista =new ArrayList<String>();
        List<String> listb =new ArrayList<String>();

        lista.add("Isabella");
        lista.add("Angelina");
        lista.add("Pille");
        lista.add("Hazem");

        listb.add("Isabella");
        listb.add("Angelina");
        listb.add("Bianca");

        // Create an aplusb list which will contain both list (list1 and list2) in which common element will occur twice 
        List<String> listapluslistb =new ArrayList<String>(lista);    
        listapluslistb.addAll(listb);

        // Create an aunionb set which will contain both list (list1 and list2) in which common element will occur once
        Set<String> listaunionlistb =new HashSet<String>(lista);
        listaunionlistb.addAll(listb);

        for(String s:listaunionlistb)
        {
            listapluslistb.remove(s);
        }
        System.out.println(listapluslistb);
	    
		

	}

}
