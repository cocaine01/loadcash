package practice;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import org.testng.annotations.Test;

public class Palindrome {

	public static void main(String[] args) throws IOException {

		String rev = "";
        
		System.out.println("Enter the word to find out if its a Palindrome");
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		String s1 = br.readLine();

		for (int i = s1.length() - 1; i >= 0; i--) {

			rev = rev + s1.charAt(i);
		}
		System.out.println(rev);
		if (s1.equals(rev)) {
			System.out.println(s1 + " is a palindrome");
		} else
			System.out.println(s1 + " is not a palindrome");

	}
}
