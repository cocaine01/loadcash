package practice;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class ReverseArray {

	public static void main(String[] args) {
		int [] arr = {1,2,3,4};
		
		//Defining a List
	   List<Object> al = new ArrayList<Object>();
	   
	   //Backward additing to LIst
		for(int i=arr.length-1;i>=0;i--){
			 al.add(arr[i]);
		}
       Iterator<Object> itr = al.iterator();
       while(itr.hasNext()){
    	   System.out.println(itr.next());
       }
	}

}
