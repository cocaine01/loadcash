package Programs;

import java.io.FileInputStream;
import java.io.IOException;

public class Variant {


		public static void main(String[] args) throws IOException {
			
			FileInputStream fis = new FileInputStream(System.getProperty("user.dir")+"\\Data\\Programs.txt");
			
			int data =fis.read(); ///byte stream coming into data
			//System.out.println((char)data);
			
			StringBuffer sb = new StringBuffer();		
			
			while (data!= -1){
				char c= ((char)data);///it takes only the first character and goes on in infinite Loop
				//System.out.println(c);
				data =fis.read();///thats why increment
				
				sb.append(c);
				
				}
	        System.out.println(sb);

	        
	        String [] arr =  sb.toString().split(" ");
	        
	        for (int i = 0; i<arr.length; i++){
				if (arr[i].getClass().getSimpleName().equalsIgnoreCase("String")){
					System.out.println("Array has number: " + arr[i]);
				}
			}
	        
	}

}
