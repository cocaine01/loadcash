package Programs;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Arrays;
import java.util.Iterator;
import java.util.Set;
import java.util.TreeSet;

import org.apache.commons.exec.util.StringUtils;

public class ReadFileInputAscendingOrder {

	public static void main(String[] args) throws IOException {
		
		FileInputStream fis = new FileInputStream(System.getProperty("user.dir")+"\\Data\\Programs.txt");
		
		int data =fis.read(); ///byte stream coming into data
		//System.out.println((char)data);
		
		StringBuffer sb = new StringBuffer();		
		
		while (data!= -1){
			char c= ((char)data);///it takes only the first character and goes on in infinite Loop
			//System.out.println(c);
			data =fis.read();///thats why increment
			
			sb.append(c);
			
			}
        System.out.println(sb);
        
        System.out.println("=========Now SORTING Steps===============");
        
        //SORTING in Ascending Order ---1st method
        
                        
           String [] arr =  sb.toString().split(" ");
//               
//               for(String str:  arr){
//            	   System.out.println(str);
//               }
               
//               Arrays.sort(arr);               
//               System.out.println("\\n");
//               for(String str:  arr){
//            	   System.out.println(str);
//               }
        //2nd Method
               
               Set<String> ts = new TreeSet<>();
               
               for (String str : arr){
            	   
            	   
            	   ts.add(str);
            	   
               }
               System.out.println(ts);
               
               //Using Iterator to display the words in a nice manner
               Iterator<String> itr = ts.iterator();
               
               while (itr.hasNext()){
            	   System.out.println(itr.next());
               }
               
               
               
	}
	
	/*
	 * String path= "C:\\Users\\sumeetk\\Desktop\\Selenium\\GIT Local Repository\\Excel\\123\\data\\Programs.txt";
	  File src= new File(path);
	  FileInputStream fis = new  FileInputStream(path);  
	  
	  StringBuffer sb = new StringBuffer();	  
	  
	  int num = fis.read();	  
	  System.out.println(num);
	  
	  while(num!=-1){
		   char c= ((char)num);
		   num= fis.read();		   
		   sb.append(c);		      
 	  }
	  System.out.println(sb);
	  System.out.println("=========Now SORTING Steps===============");
	  Set<String> tm = new TreeSet<String>();
	  String [] arr= sb.toString().split(" ");
	 
	 for (String final_str : arr){
		 tm.add(final_str);
	 }
	 System.out.println(tm);
	  
	 Iterator<String> itr = tm.iterator();
	 
	 while(itr.hasNext()){
		 System.out.println(itr.next());
	 }		
	 */
	 

}
