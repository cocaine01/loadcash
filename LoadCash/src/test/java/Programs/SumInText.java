package Programs;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class SumInText {

	public static void main(String[] args) throws FileNotFoundException {
		Scanner scn = new Scanner(new FileInputStream(System.getProperty("user.dir")+"\\Data\\Programs.txt"));
		int sum=0;
		while(scn.hasNext()){
			if(scn.hasNextInt()){
				sum=sum+scn.nextInt();
			}
			else{
				scn.next();
			}
		}
		System.out.println("Sum is"+sum);		
		
	}

	

}
