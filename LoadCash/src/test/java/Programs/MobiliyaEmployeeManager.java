package Programs;

import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map.Entry;
import java.util.Set;

public class MobiliyaEmployeeManager {
	
	public static void main(String[] args) {
		
        HashMap<String, String> hm = new HashMap<String, String>();		
		hm.put("A", "C");
		hm.put("B", "C");
		hm.put("C", "F");
		hm.put("D", "E");
		hm.put("E", "F");
		hm.put("F", "F");		
		
		System.out.println(hm);
		HashMap<String, Integer> hm_relation = new HashMap<String, Integer>();		
		//for the Keys:-
		Collection<String> keys = hm.keySet();	
		Iterator<String> Kitr = keys.iterator();		
		while(Kitr.hasNext()){			
			String key_v = Kitr.next(); 			
			hm_relation.put(key_v, 0);			
		}
		System.out.println(hm_relation);
		
		//for the Values
		Collection<String> values = hm.values();
		Iterator<String> Vitr = values.iterator();		
		while(Vitr.hasNext()){
			String value_v= Vitr.next();
			//***It is appending the values, the return type of PUT is object
			//put(c,0+1)., baki A ke value ko wo nahi chhuta 
			hm_relation.put(value_v,hm_relation.get(value_v) + 1 );
		}
   
		System.out.println(hm_relation);
		
		Set<String> keyset= hm_relation.keySet();
		Iterator<String> itr = keyset.iterator();
		
		for(Entry<String, Integer> mp : hm_relation.entrySet()){
			System.out.println(mp.getKey()+"------>"+mp.getValue());
		}
		
	}
}

	/*public static void main(String[] args) {
		
		HashMap<String, String> hm = new HashMap<String, String>();
		
		hm.put("A", "C");
		hm.put("B", "C");
		hm.put("C", "F");
		hm.put("D", "E");
		hm.put("E", "F");
		hm.put("F", "F");
		
		System.out.println(hm);
				
		Set<Entry<String,String>> set = hm.entrySet();
		Iterator<Entry<String,String>> itr = set.iterator();
		
		HashMap<String, Integer> hm_relation = new HashMap<String, Integer>();
		
		while(itr.hasNext()){
			
			Entry<String, String> e = itr.next();			
				String emp = e.getKey();				
				hm_relation.put(emp, 0);
				
		}
		
		Collection<String> values = hm.values();		
		Iterator<String> vitr = values.iterator();		
		while(vitr.hasNext()){			
			String str = vitr.next(); 			
			hm_relation.put(str, hm_relation.get(str) + 1);
			
		}
		
		
		System.out.println(hm_relation);
		
		

	}

}*/



