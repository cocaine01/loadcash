package Programs;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;

public class ReadLastNLines {

	public static void main(String[] args) throws Exception {
		ArrayList<String> bandWidth = new ArrayList<String>();
		FileInputStream in = new FileInputStream(System.getProperty("user.dir")+"\\Data\\Last5Lines.txt");
		BufferedReader br = new BufferedReader(new InputStreamReader(in));

		String tmp;
		while ((tmp = br.readLine()) != null)
		{
			bandWidth.add(tmp);
			if (bandWidth.size() == 6)
				bandWidth.remove(0);
		}

		ArrayList<String> reversedFive = new ArrayList<String>();
		for (int i = bandWidth.size() - 1; i >= 0; i--){
			reversedFive.add(bandWidth.get(i));
			in.close();

		}
		System.out.println(reversedFive);

	}

}
