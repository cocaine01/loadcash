package Programs;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;

public class SearchForStringAndPrintLine {
	
	public static void grep(Reader string, String searchFor) throws IOException {
	    BufferedReader reader = null;
	    try {
	        reader = new BufferedReader(string);
	        String line;
	        while ((line = reader.readLine()) != null) {
	            if (line.contains(searchFor)) {
	                System.out.println(line);
	            }
	        }
	    } finally {
	        if (reader != null) {
	            reader.close();
	        }
	    }
	}

	public static void main(String[] args) throws FileNotFoundException, IOException {
		
		grep(new FileReader(System.getProperty("user.dir")+"\\Data\\output.txt"),"2.6");
	}

}
