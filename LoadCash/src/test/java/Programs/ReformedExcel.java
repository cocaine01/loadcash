package Programs;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class ReformedExcel {
	
	File src = null;
	FileInputStream fis = null;
	
    Map<Object ,Object> hm =new HashMap<Object ,Object>();
	
	
	
	public void ReadExcel(String file_path , String file_name ,String sheet_name  ) throws IOException{
		
		String path = file_path+"\\"+file_name ;
		
		src = new File(path);
		fis = new FileInputStream(src);
		
		String file_extension = file_name.substring(file_name.indexOf("."));

		if(file_extension.equalsIgnoreCase(".xlsx")){
			XSSFWorkbook wb = new XSSFWorkbook(fis);
			XSSFSheet sh= wb.getSheet(sheet_name);
			
			int row_count = sh.getLastRowNum()-sh.getFirstRowNum();
			System.out.println("row count is " +row_count); 
			
			for ( int i=1;i<=row_count;i++){
				Row rw= sh.getRow(i);
				
				for (int j=0;j<=rw.getLastCellNum();j++){
					hm.put(rw.getCell(j).getStringCellValue() ,rw.getCell(j+1).getStringCellValue() );
					break;
				}
			}			
			
		}
		if(file_extension.equalsIgnoreCase(".xls")){
			HSSFWorkbook wb = new HSSFWorkbook(fis);
			HSSFSheet sh= wb.getSheet(sheet_name);
			int row_count = sh.getLastRowNum()-sh.getFirstRowNum();
			System.out.println("row count is " +row_count); 
			
			for ( int i=1;i<row_count;i++){
				Row rw= sh.getRow(i);
				
				for (int j=0;j<=rw.getLastCellNum();j++){
					hm.put(rw.getCell(j).getStringCellValue() ,rw.getCell(j+1).getStringCellValue() );
					break;
				}				
			}				
		}
		///Retrieving the values/////
		Set <Object> keySet = hm.keySet();
		Iterator<Object> itr = keySet.iterator();
		
	    for(Map.Entry<Object, Object> mp : hm.entrySet()){
	    	System.out.println(mp.getKey()+"   "+mp.getValue());
	    }
		
		
	}
	
	public static void main(String[] args) throws IOException {
		ReformedExcel rs= new ReformedExcel();
		rs.ReadExcel("C:\\Users\\sumeetk\\Desktop\\Selenium\\GIT Local Repository\\Excel\\123\\data", "dataSheet.xlsx", "Sheet1");
		
		
	}
	


}