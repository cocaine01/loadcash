package Selenium;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.interactions.Actions;
import org.testng.Assert;

public class ToolTip {

	public static void main(String[] args) {
		
		
		System.setProperty("webdriver.gecko.driver", System.getProperty("user.dir")+"\\drivers\\geckodriver.exe");
		WebDriver driver = new FirefoxDriver();
		
	    driver.get("https://jqueryui.com/tooltip/")	;
	    
	    driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
	    
	    driver.switchTo().frame(driver.findElement(By.xpath("//iframe[@class='demo-frame']")));
	    
	    WebElement element = driver.findElement(By.id("age"));
	    
	    System.out.println("Sumeet");
	    
	    
	    Actions action= new Actions(driver);
	    action.moveToElement(element).click().build().perform();
	    WebElement toolTipElement = driver.findElement(By.cssSelector(".ui-tooltip"));

		// To get the tool tip text and assert
		String toolTipText = toolTipElement.getText();
		Assert.assertEquals("We ask for your age only for statistical purposes.", toolTipText);
		
		System.out.println("RK Wins");

	}

}
