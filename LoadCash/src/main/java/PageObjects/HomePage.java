package PageObjects;


import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.interactions.Actions;

import LoadCash.LoadCash.Base;
import Utilities.Helper;

public class HomePage extends Base {
	
	WebDriver driver = null;	
	
	
	public HomePage(WebDriver driver){
		this.driver=driver;
	}
	
	public boolean LoadCashMtd() throws Exception{
//		Helper.ExplicitWait("");
//		SelWebEleFromOR("", "click");
//		
		//driver.findElement(By.xpath(OR.getProperty("lnk_CybageOnlineStore"))).click();
		SelWebEleFromOR("lnk_CybageOnlineStore", "click");
		Thread.sleep(5000);
		
		Helper.switchToWindowID(2);
		Thread.sleep(5000);	
		//Runtime.getRuntime().exec("C:\\Users\\sumeetk\\Desktop\\Selenium\\Auto IT\\Load Cash\\AuthenticationSom.exe");
	
		Runtime.getRuntime().exec(System.getProperty("user.dir")+"\\AutoITScripts\\AuthenticationSom.exe");
		
		
		Helper.ExplicitWait("lnk_LoadCash");
		SelWebEleFromOR("lnk_LoadCash", "click");
		
		Actions act = new Actions(driver);
		act.moveToElement(driver.findElement(By.xpath(OR.getProperty("lnk_1000_buy_now")))).click().build().perform();
		
		
//		Helper.ExplicitWait("lnk_1000_buy_now");
//		SelWebEleFromOR("lnk_1000_buy_now", "click");
		
		//Helper.AlertAccept();
		
		Thread.sleep(5000);
		
		Helper.ExplicitWait("buy_now_link_btn");
		SelWebEleFromOR("buy_now_link_btn", "click");
		
		
		Helper.ExplicitWait("btn_proceedToCheckoutButton");
		SelWebEleFromOR("btn_proceedToCheckoutButton", "click");
		
		Helper.ExplicitWait("btn_bill_here");
		SelWebEleFromOR("btn_bill_here", "click");
		
		Helper.ExplicitWait("btn_continue");
		SelWebEleFromOR("btn_continue", "click");
		
		Helper.ExplicitWait("Radiobtn_creditCard");
		SelWebEleFromOR("Radiobtn_creditCard", "click");
		
		Helper.ExplicitWait("btn_ProceedToPayment");
		SelWebEleFromOR("btn_ProceedToPayment", "click");
		
		//Values:-
		Helper.ExplicitWait("txt_CardNumber");
		sendKeys("txt_CardNumber", "CardNumber");
		
		Helper.ExplicitWait("txt_Name");
		sendKeys("txt_Name", "Name");
		
		Helper.ExplicitWait("txt_CCV");
		sendKeys("txt_CCV", "CCV");	
		
		
		
		return true;
	}	
	
	
}
