package Utilities;

import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.openqa.selenium.Alert;
import LoadCash.LoadCash.Base;

public class Helper extends Base{
	
//	public static boolean ExplicitWait(String xpath){
//		WebDriverWait  wait = new WebDriverWait(driver, 30);
//		WebElement ele =wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(xpath)));
//		
//		if(ele.isDisplayed()){
//			return true;
//		}
//		else {
//			return false;
//		}
//	}
	
	   public static boolean ExplicitWait(String elementOR){
			WebDriverWait wait = new WebDriverWait(driver,30);
			wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath(OR.getProperty(elementOR)))));
			return true;
		}
	
	//Window Handling
    public static void switchToWindowID(int index){    	
    	String windowID = null;    	
    	Set<String> windowIDS= driver.getWindowHandles();
    	Iterator<String> itr = windowIDS.iterator();    	
    	for(int i=1;i<=index;i++){
    		windowID=itr.next();    		
    	}
    	driver.switchTo().window(windowID);
    }
    
    public static void AlertAccept(){
    	Alert alert = driver.switchTo().alert();
    	alert.accept();
    	
    }
    

    
    
	
}
