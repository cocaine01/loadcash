package LoadCash.LoadCash;

import java.io.File;
import java.io.FileInputStream;
import java.util.Properties;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.ie.InternetExplorerOptions;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class Base {

	public static Properties CONFIG = null;
	public static Properties OR = null;
	public static WebDriver driver = null;
	
	

	//**********Configuration file*****************
	public static void loadProperties() throws Exception{		
		FileInputStream fis1 = new FileInputStream(new File(System.getProperty("user.dir")+"\\config\\config.properties"));		
		CONFIG = new Properties();
		CONFIG.load(fis1);
		fis1.close();
		
		FileInputStream fis2 = new FileInputStream(new File(System.getProperty("user.dir")+"\\config\\OR.properties"));		
		OR = new Properties();
		OR.load(fis2);
		fis2.close();
		
	}
	
	public static void SelWebEleFromOR(String elementOR ,String action){
		
		if(action.equalsIgnoreCase("click")){
			driver.findElement(By.xpath(OR.getProperty(elementOR))).click();
		}
		if(action.equalsIgnoreCase("clear")){
			driver.findElement(By.xpath(OR.getProperty(elementOR))).clear();
		}
//		if(action.equalsIgnoreCase("sendKeys")){
//			driver.findElement(By.xpath(OR.getProperty(elementOR))).sendKeys(OR.getProperty(elementORvalue));
//		}
	}
	public static void sendKeys(String elementOR, String elementORValue){
		driver.findElement(By.xpath(OR.getProperty(elementOR))).sendKeys(OR.getProperty(elementORValue));
	}
	
 
	
	

	///**********Browser Initialization********************
	public static WebDriver getBrowser(String browser){
		if(browser.equalsIgnoreCase("Chrome")){
			System.setProperty("webdriver.chrome.driver" , System.getProperty("user.dir")+"\\drivers\\chromedriver.exe");
			ChromeOptions options = new ChromeOptions();
			options.addArguments("disable-infobars");
			options.addArguments("--start-maximized");
			options.addArguments("--ignore-certificate-errors");
			options.addArguments("--disable-popup-blocking");
			options.addArguments("--incognito");
			options.addArguments("disable-notifications");			
			driver= new ChromeDriver(options);	

		}
		if(browser.equalsIgnoreCase("InternetExplorer")){

			System.setProperty("webdriver.ie.driver", System.getProperty("user.dir")+"\\drivers\\IEDriverServer.exe");
			InternetExplorerOptions options = new InternetExplorerOptions();
			options.setCapability("ie.ensureCleanSession", true);
			options.setCapability(CapabilityType.ACCEPT_SSL_CERTS, true);
			driver = new InternetExplorerDriver(options);
		}
		return driver;
	}



}
